#!/bin/bash
# backup script
# Clément - 24/10/2021

#on set le nom du fichier
dates=$(date +'%Y-%m-%d-%H-%M-%S')
my_archive=tp2_backup_$dates.tar.gz

#on compress
tar czf $my_archive $2
#$2 pour récup le 2e argument rentrée

#on envoie la sauvegarde
rsync -a $my_archive $1