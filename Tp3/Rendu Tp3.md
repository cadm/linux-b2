# TP3 : Your own shiet
 
# Sommaire
 
- [TP3 : Your own shiet](#tp3-:-your-own-shiet)
- [Sommaire](#sommaire)
- [0. Description de la solution à déployer](#0-Description-de-la-solution-à-déployer)
- [I. Prérequis](#i-prérequis)
- [II. Déploiement de la solution ](#ii-Déploiement-de-la-solution)
  - [1. Etape 1 : Téléchargement](##1-Etape-1-:-Téléchargement)
  - [2. Etape 2 : Installations](##2-Etape-2-:-Installations)
  - [3. Etape 3 : Configuration](##3-Etape-3-:-Configuration)
- [III. Final](#iii-III.-Final)
 
 
# 0. Description de la solution à déployer
 
Déploiement d'une application génératrice de QrCode lorsque l'on lui rentre une url.  
L'interface graphique est gérée à l'aide de pages web.  
Un script permet de sauvegarder sur un serveur les Qr Codes générés.  
Un service de monitoring de notre application web est également en place.
 
# I. Prérequis
 
Voici la liste des prérequis avant l'installation de ce projet :
 
➜ 2 Machines Rocky Linux
 
➜ 1 user par machines
 
➜ Celles-ci doivent avoir une IP statique (voir tableau d'adressage plus bas)
 
➜ Définir leurs hostname (voir tableau d'adressage plus bas)
 
➜ Un accès Internet est nécessaire
 
Avant de se lancer dans l'installation, veuillez vérifier que les prérequis soient effectués.
 
**Tableau d'adressage des IP**
| Machine            | IP           | Service                           | Port ouvert |
|--------------------|--------------|-----------------------------------|-------------|
| `serverWeb.tp3`    | `10.10.1.13` | Serveur Web (Flask)               | 5000        |
| `serverBackUp.tp3` | `10.10.1.14` | Serveur de back-up des QrCode     | .           |
 
# II. Déploiement de la solution
 
## 1. Etape 1 : Téléchargement
 
Veuillez télécharger les 3 scripts :
- ScriptInstalationServerWeb.sh
- ScriptBackUpForServerWeb.sh
- ScriptBackUpForBackUpServer.sh
 
Maintenant rendons les scripts exécutable (commandes à lancer à l'aide de `sudo`):  
```
sudo chmod x ScriptInstalationServerWeb.sh
sudo chmod x ScriptBackUpForServerWeb.sh
sudo chmod x ScriptBackUpForBackUpServer.sh
```
 
## 2. Etape 2 : Installations
 
Veuillez lancer les scripts à l'aide de `sudo`:  
`exemple : sudo ./ScriptInstalationServerWeb.sh`.
 
Nous commençons par installer le serveur web sur la machine `serverWeb.tp3`.  
Veuillez à présent lancer les scripts en suivant l'ordre :    
**1.** ==> ScriptInstalationServerWeb.sh  
**2.** ==>ScriptBackUpForServerWeb.sh
 
Installez le serveur de back-up sur la machine `serverBackUp.tp3`.  
Lancer le script suivant :  
**3.** ==> ScriptInstalationServerWeb.sh
 
## 3. Etape 3 : Configuration
Après avoir installer les 3 scripts
Vous aurez une configuration comme celle-ci :
- Pour serverWeb.tp3
``` tree
├── flask-project
    ├── apiQrCode.py
    ├── config.cfg
    ├── flask_monitoringdashboard.db
    ├── QrCodeGenere.png
    ├── app.py
    ├── env
    ├── __pycache__
    │   ├── apiQrCode.cpython-36.pyc
    │   └── app.cpython-36.pyc
    ├── templates
    │   ├── generateQrCode.html
    │   └── index.html
    └── ScriptBackUpForServerWeb.sh
```
 
- Pour serverBackUp.tp3
```tree
.
└── backUpQrCode
    └── QrCodeGenere.png
```
Aucune configuration de plus n'est nécessaire.
 
 
# III. Final
Le service étant lancé, afin de créer votre rendez vous à : http://10.10.1.13:5000/home
*Page 1 ↓*:![](https://i.imgur.com/NqAMjM5.jpg)

-------------------------------------------------------
Lorsque vous cliquez sur le bouton vous arrivez sur une page vous permettant de télécharger votre qr code en png
*Page 2 ↓*![](https://i.imgur.com/1yeOAur.jpg)
  
On obtient un QrCode !!!  
Si l'on scanne celui-ci, il nous renvoie bien vers l'url rentrée soit google.com
![](https://i.imgur.com/pevndI5.png)


-------------------------------------------------------
Pour passer du côté monitoring rendez-vous sur la page : http://10.10.1.13:5000/dashboard/overview
*Page 3 ↓*![](https://i.imgur.com/BJDHLTI.jpg)

