#!/bin/bash
# This script install a service web name Flask on your computer
# The website will let yu convert a url into a QrCode. You can also monitore your web site.
# Clement ~ 11/11/2021



# Colors
G="\e[32m"
R="\e[31m"

# We update the computer
dnf update -y
echo -e "${G}Update de la machine effectuer"

# We manage the firewall
firewall-cmd --permanent --add-port 5000/tcp
firewall-cmd --remove-service=dhcpv6-client
firewall-cmd --reload
echo -e "${G}The rules of the firewall have been changed"

# Installing packages
dnf install python3
pip3 install pyqrcode
echo -e "${G}Python and pyqrcode have been installed"

# Establishment of ours environment 
mkdir flask-project && cd flask-project
echo -e "${G}You are now into the directorie named flask-project"
python3 -m venv env
source env/bin/activate
echo -e "${G}You are now into a python environnement"

pip install Flask
pip install pyqrcode
pip install flask_monitoringdashboard
echo -e "${G}Flask, pyqrcode and flask_monitoringdashboard have been installed into the python environnement" 

# Création of all the file for the website
echo "
import pyqrcode

def QrCode(urlUser):
    generateqr = pyqrcode.create(urlUser)
    generateqr.png('QrCodeGenere.png',scale = 8)" > apiQrCode.py

echo "apiQrCode.py have been created"

mkdir templates && cd templates/
echo -e "${G}Your are now into the directorie named templates"

echo "
<!DOCTYPE html>
<html lang="fr">
        <head>
        <style>
                body{
                        text-align: center
                }
                button{
                        background-color: #008CBA;
                        border: none;
                        color: white;
                        padding: 32px 16px;
                        font-size: 16px;
                        border-radius: 60%;
                }
        </style>
        </head>
        <body bgcolor="33FFF3">
                <form action="/download" method="get">
                        <h5> </h5>
                        <h5> </h5>
                        <h5> </h5>
                        <h1>Le Qr code a été convertit avec succès</h1>
                        <h5> </h5>
                        <h5> </h5>
                        <h5>Télécharger le Qr Code</h5>
                        <h1>&#128419;</h1>
                        <button class="btn"><i class="fa fa-download"></i>Télécharger</button>
                </form>                                                                                                                               
        </body>
</html> " > generateQrCode.html
echo -e "${G}generateQrCode.html have been created"

echo " 
<!DOCTYPE html>
<html >
        <head>
        <style>
                body{
                        text-align: center
                }
        </style>
        </head>
        <body bgcolor="33FFF3">
                <div class="login">
                        <h1>QR code Generater</h1>
                        <h5> </h5>
                        <h5> </h5>
                        <h5>Bienvenue !</h5>
                        <h5> </h5>
                        <h5>Ce site te permet de transformer une url en un QrCode</h5>
                        <h5>Merci de renter une url dans l'encadré ci-dessous</h5>
                        <h5> </h5>
                        <form action = "/generateQrCode" method="post">
                                <input type="text" name="test" placeholder="Entrez une URL" required="required" />
                                <button type="submit" class="btn btn-primary btn-block btn-large">Génération du QRCode</button>
                        </form>
                </div>" > index.html

echo -e "${G}index.html have been created"

cd ..

echo "
from apiQrCode import QrCode
    from flask import  Flask,render_template
    from flask import *

    import flask_monitoringdashboard as dashboard

    app = Flask(__name__)

    dashboard.config.init_from(file='/config.cfg')
    dashboard.bind(app)

    @app.route('/home')
    def home():
            return render_template('index.html')


    @app.route('/generateQrCode',methods = ['POST'])
    def convert():
            global text
            text = request.form['test']
            return render_template('generateQrCode.html')


    @app.route('/download')
    def download():
            QrCode(text)
            nomfichier = 'QrCodeGenere.png'
            return send_file(nomfichier, as_attachment=True)

    if __name__ == "__main__":
            app.run(debug=True)" > app.py

echo -e "${G}app.py have been created"

echo " 
    [dashboard]
    APP_VERSION=1.0
    CUSTOM_LINK=dashboard
    MONITOR_LEVEL=3
    OUTLIER_DETECTION_CONSTANT=2.
    [authentication]
    USERNAME=admin
    PASSWORD=admin
    GUEST_USERNAME=guest
    GUEST_PASSWORD=['dashboardguest!', 'second_pw!']
    SECURITY_TOKEN=cc83733cb0af8b884ff6577086b87909
    [database]
    DATABASE=sqlite:///flask_monitoringdashboard.db
    [visualization]
    TIMEZONE=Europe/Stockholm
    COLORS={'main':'[0,97,255]', 'static':'[255,153,0]'}" > config.cfg
echo -e "${G}config.cfg have been created"

flask run -h 10.10.1.13 -p 5000
echo -e "${G}The web serveur is running"
echo "Go to http://10.10.1.13:5000/home for the QrCode generator service"
echo "Go to http://10.10.1.13:5000/dashboard/overview to monitore your service"