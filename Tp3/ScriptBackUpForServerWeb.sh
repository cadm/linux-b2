#!/bin/bash
# This script send all png file to a server at the ip 10.10.1.14
# Clement ~ 11/11/2021

# Colors
G="\e[32m"
R="\e[31m"

echo "
    #!/bin/bash
    while (true)
    do
            for FILE in /home/clement/flask-project/*.png
            do
                    scp clement@10.10.1.13:$FILE clement@10.10.1.14:/home/clement/backUpQrCode
                    sleep 3600
            done
            echo "Fichiers transférés sur la machine 10.10.1.14"
    done" > scriptBackUp.sh

echo -e "${G}scriptBackUp.sh have been created"

chmod +x scriptBackUp.sh

./scriptBackUp.sh
echo -e "${R}scriptBackUp.sh is now turning into the background. Every hours you must put back your password"