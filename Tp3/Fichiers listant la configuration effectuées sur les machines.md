# 0. Commandes rentrées sur la machine serverWeb.tp3 
## Partie 1
```linux
[clement@localhost ~]$ sudo dnf update -y

[clement@localhost ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.10.1.13
NETMASK=255.255.255.0

[clement@localhost ~]$ sudo nmcli con reload

[clement@localhost ~]$ sudo nmcli con up enp0s8

[clement@localhost ~]$ sudo vim /etc/hostname

[clement@localhost ~]$ sudo firewall-cmd --permanent --add-port 5000/tcp

[clement@localhost ~]$ sudo firewall-cmd --reload

[clement@serverWeb ~]$ sudo dnf install python3

[clement@serverWeb ~]$ sudo pip3 install pyqrcode

[clement@serverWeb ~]$ mkdir flask-project && cd flask-project

[clement@serverWeb flask-project]$ python3 -m venv env

[clement@serverWeb flask-project]$ source env/bin/activate

(env) [clement@serverWeb flask-project]$ pip install Flask

(env) [clement@serverWeb flask-project]$ pip install pyqrcode

(env) [clement@serverWeb flask-project]$ pip install flask_monitoringdashboard

(env) [clement@serverWeb flask-project]$ vim apiQrCode.py

(env) [clement@serverWeb flask-project]$ cat apiQrCode.py
import pyqrcode

def QrCode(urlUser):
    generateqr = pyqrcode.create(urlUser)
    generateqr.png('QrCodeGenere.png',scale = 8)

(env) [clement@serverWeb flask-project]$ sudo mkdir templates
(env) [clement@serverWeb flask-project]$ cd templates/
(env) [clement@serverWeb templates]$ sudo vim generateQrCode.html
(env) [clement@serverWeb templates]$ cat generateQrCode.html
<!DOCTYPE html>
<html lang="fr">
        <head>
        <style>
                body{
                        text-align: center
                }
                button{
                        background-color: #008CBA;
                        border: none;
                        color: white;
                        padding: 32px 16px;
                        font-size: 16px;
                        border-radius: 60%;
                }
        </style>
        </head>
        <body bgcolor="33FFF3">
                <form action="/download" method="get">
                        <h5> </h5>
                        <h5> </h5>
                        <h5> </h5>
                        <h1>Le Qr code a été convertit avec succès</h1>
                        <h5> </h5>
                        <h5> </h5>
                        <h5>Télécharger le Qr Code</h5>
                        <h1>&#128419;</h1>
                        <button class="btn"><i class="fa fa-download"></i>Télécharger</button>
                </form>                                                                                                                               </body>
</html>

(env) [clement@serverWeb templates]$ sudo vim index.html
[sudo] password for clement:

(env) [clement@serverWeb templates]$ cat index.html
<!DOCTYPE html>
<html >
        <head>
        <style>
                body{
                        text-align: center
                }
        </style>
        </head>
        <body bgcolor="33FFF3">
                <div class="login">
                        <h1>QR code Generater</h1>
                        <h5> </h5>
                        <h5> </h5>
                        <h5>Bienvenue !</h5>
                        <h5> </h5>
                        <h5>Ce site te permet de transformer une url en un QrCode</h5>
                        <h5>Merci de renter une url dans l'encadré ci-dessous</h5>
                        <h5> </h5>
                        <form action = "/generateQrCode" method="post">
                                <input type="text" name="test" placeholder="Entrez une URL" required="required" />
                                <button type="submit" class="btn btn-primary btn-block btn-large">Génération du QRCode</button>
                        </form>
                </div>

(env) [clement@serverWeb templates]$ cd ..


(env) [clement@restart flask-project]$ vim config.cfg

(env) [clement@restart flask-project]$ cat config.cfg
    [dashboard]
    APP_VERSION=1.0
    CUSTOM_LINK=dashboard
    MONITOR_LEVEL=3
    OUTLIER_DETECTION_CONSTANT=2.
    [authentication]
    USERNAME=admin
    PASSWORD=admin
    GUEST_USERNAME=guest
    GUEST_PASSWORD=['dashboardguest!', 'second_pw!']
    SECURITY_TOKEN=cc83733cb0af8b884ff6577086b87909
    [database]
    DATABASE=sqlite:///flask_monitoringdashboard.db
    [visualization]
    TIMEZONE=Europe/Stockholm
    COLORS={'main':'[0,97,255]', 'static':'[255,153,0]'}

(env) [clement@serverWeb flask-project]$ sudo vim app.py

(env) [clement@serverWeb flask-project]$ cat app.py
from apiQrCode import QrCode
    from flask import  Flask,render_template
    from flask import *

    import flask_monitoringdashboard as dashboard

    app = Flask(__name__)

    dashboard.config.init_from(file='/config.cfg')
    dashboard.bind(app)

    @app.route('/home')
    def home():
            return render_template('index.html')


    @app.route('/generateQrCode',methods = ['POST'])
    def convert():
            global text
            text = request.form['test']
            return render_template('generateQrCode.html')


    @app.route('/download')
    def download():
            QrCode(text)
            nomfichier = 'QrCodeGenere.png'
            return send_file(nomfichier, as_attachment=True)

    if __name__ == "__main__":
            app.run(debug=True)

(env) [clement@serverWeb flask-project]$ flask run -h 10.10.1.13 -p 5000
```

## Partie 2

N'ayant pas réussi à mettre en place un serveur nfs ou rsync je suis parti sur une script bash à la main ou l'utilisateur doit rentrer le mot de passe de sa machine et du serveur de backup.  
Ce script se lance toutes les heures et l'utilisateur doit rentrer son mot de passe chaque heure (**PS : Je n'ais pas trouver d'autres moyens sans que le mot de passe soit sauvegardé textuellement dans le sript bash : ce qui n'est pas du tout sécurisé**)
```bash
(env) [clement@serverWeb flask-project]$ sudo vim test.sh
(env) [clement@serverWeb flask-project]$ sudo cat test.sh
    #!/bin/bash
    while (true)
    do
            for FILE in /home/clement/flask-project/*.png
            do
                    scp clement@10.10.1.13:$FILE clement@10.10.1.14:/home/clement/backUpQrCode
                    sleep 3600
            done
            echo "Fichiers transférés sur la machine 10.10.1.14"
    done
(env) [clement@serverWeb flask-project]$ sudo chmod +x test.sh
(env) [clement@serverWeb flask-project]$ sudo ./test.sh
```


# I. Commandes rentrées sur la machine serverBackUp.tp3
```
[clement@localhost ~]$ sudo vim /etc/hostname

[clement@serverBackUp ~]$ sudo dnf update -y

[clement@serverBackUp ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.10.1.14
NETMASK=255.255.255.0

[clement@serverBackUp ~]$ sudo nmcli con reload

[clement@serverBackUp ~]$ sudo nmcli con up enp0s8

[clement@serverBackUp ~]$ mkdir backUpQrCode
```
