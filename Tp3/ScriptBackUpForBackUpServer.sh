#!/bin/bash
# This script prepare this environnement before receiving the back-up of the server web
# Clement ~ 11/11/2021

# Colors
G="\e[32m"
R="\e[31m"

dnf update -y
echo -e "${G}The computer have been updated"

firewall-cmd --remove-service=dhcpv6-client
firewall-cmd --reload
echo -e "${G}The rules of the firewall have been changed"

mkdir backUpQrCode
echo -e "${G}The directorie backUpQrCode have been created"