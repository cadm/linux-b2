# TP2 pt. 1 : Gestion de service

Dans ce TP on va s'orienter sur l'**utilisation des systèmes GNU/Linux** comme un outil pour **faire tourner des services**. C'est le principal travail de l'administrateur système : fournir des services.

Ces services, on fait toujours la même chose avec :

- **installation** (opération ponctuelle)
- **configuration** (opération ponctuelle)
- **maintien en condition opérationnelle** (opération continue, tant que le service est actif)

**Dans cette première partie, on va voir la partie installation et configuration.** Peu importe l'outil visé, de la base de données au serveur cache, en passant par le serveur web, le serveur mail, le serveur DNS, ou le serveur privé de ton meilleur jeu en ligne, c'est toujours pareil : install into conf.

La partie maintien en condition opérationnelle sera abordée dans la deuxième partie de ce TP.

**On va apprendre à maîtriser un peu ces étapes, et pas simplement suivre la doc.**

On va maîtriser le service fourni :

- manipulation du service avec systemd
- quel IP et quel port il utilise
- quels utilisateurs du système sont mobilisés
- quels processus sont actifs sur la machine pour que le service soit actif
- gestion des fichiers qui concernent le service et des permissions associées
- gestion avancée de la configuration du service

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a-serveur-web-et-nextcloud)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# 0. Prérequis

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.102.1.0/24`.**

➜ Chaque **création de machines** sera indiqué par **l'emoji 🖥️ suivi du nom de la machine**

➜ Si je veux **un fichier dans le rendu**, il y aura l'**emoji 📁 avec le nom du fichier voulu**. Le fichier devra être livré tel quel dans le dépôt git, ou dans le corps du rendu Markdown si c'est lisible et correctement formaté.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel avec un échange de clé
- [x] accès Internet (une route par défaut donc :) )
  - le routeur a une carte NAT
  - les autres machines passent par le routeur pour avoir internet (ajout de route par défaut)
- [x] résolution de nom

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.

🌞 **Installer le serveur Apache**

- paquet `httpd`
- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`

> Ce que j'entends au-dessus par "fichier de conf principal" c'est que c'est **LE SEUL** fichier de conf lu par Apache quand il démarre. C'est souvent comme ça : un service ne lit qu'un unique fichier de conf pour démarrer. Cherchez pas, on va toujours au plus simple. Un seul fichier, c'est simple.  
**En revanche** ce serait le bordel si on mettait toute la conf dans un seul fichier pour pas mal de services.  
Donc, le principe, c'est que ce "fichier de conf principal" définit généralement deux choses. D'une part la conf globale. D'autre part, il inclut d'autres fichiers de confs plus spécifiques.  
On a le meilleur des deux mondes : simplicité (un seul fichier lu au démarrage) et la propreté (éclater la conf dans plusieurs fichiers).

```
[clement@web ~]$ sudo dnf -y install httpd
```

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le
```
 systemctl start httpd
 ```

  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
```
[clement@web ~]$ sudo systemctl enable httpd
[sudo] password for clement:
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```
  - ouvrez le port firewall nécessaire
```
[clement@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache
```
[clement@web ~]$ sudo ss -alnpt
State          Recv-Q         Send-Q                  Local Address:Port                   Peer Address:Port         Process
LISTEN         0              128                           0.0.0.0:22                          0.0.0.0:*             users:(("sshd",pid=843,fd=5))
LISTEN         0              128                                 *:80                                *:*             users:(("httpd",pid=2260,fd=4),("httpd",pid=2259,fd=4),("httpd",pid=2258,fd=4),("httpd",pid=2256,fd=4))
LISTEN         0              128                              [::]:22                             [::]:*             users:(("sshd",pid=843,fd=7))
[
```
    - [une petite portion du mémo est consacrée à `ss`](https://gitlab.com/it4lik/b2-linux-2021/-/blob/main/cours/memo/commandes.md#r%C3%A9seau)

**En cas de problème** (IN CASE OF FIIIIRE) vous pouvez check les logs d'Apache :

```bash
# Demander à systemd les logs relatifs au service httpd
$ sudo journalctl -xe -u httpd

# Consulter le fichier de logs d'erreur d'Apache
$ sudo cat /var/log/httpd/error_log

# Il existe aussi un fichier de log qui enregistre toutes les requêtes effectuées sur votre serveur
$ sudo cat /var/log/httpd/access_log
```

🌞 **TEST**

- vérifier que le service est démarré
```
[clement@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 11:49:51 CEST; 22min ago
     Docs: man:httpd.service(8)
 Main PID: 2256 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4946)
   Memory: 24.8M
   CGroup: /system.slice/httpd.service
           ├─2256 /usr/sbin/httpd -DFOREGROUND
           ├─2257 /usr/sbin/httpd -DFOREGROUND
           ├─2258 /usr/sbin/httpd -DFOREGROUND
           ├─2259 /usr/sbin/httpd -DFOREGROUND
           └─2260 /usr/sbin/httpd -DFOREGROUND

Sep 29 11:49:51 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Sep 29 11:49:51 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Sep 29 11:49:51 web.tp2.linux httpd[2256]: Server configured, listening on: port 80
```

- vérifier qu'il est configuré pour démarrer automatiquement
```
[clement@web ~]$ sudo sudo systemctl is-enabled httpd
enabled
```

- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement  
L'output de la commande curl étant important je l'ais résumé
```
[clement@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

[...]

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web
Commande à taper dans mon naviguateur: ```http://10.102.1.11:80```
La page du serevur web s'affiche bien.

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
```sudo systemctl enable httpd```

- prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume
```
[clement@web ~]$ systemctl is-enabled httpd
enabled
```

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
```
[clement@web ~]$ sudo cat /usr/lib/systemd/system/httpd.service |grep "^[^#;]"
[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)
[Service]
Type=notify
Environment=LANG=C
ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
[Install]
WantedBy=multi-user.target
```
🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf qui définit quel user est utilisé
```
[clement@web httpd]$ sudo cat /etc/httpd/conf/httpd.conf | grep user -i
[...]
User apache
[...]
```

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```
[clement@web httpd]$ ps -ef | grep -i apache
apache       875     856  0 09:48 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       876     856  0 09:48 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       877     856  0 09:48 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       878     856  0 09:48 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
clement     1866    1679  0 10:43 pts/0    00:00:00 grep --color=auto -i apache
```
Le service httpd tourne bien sous l'utilisateur apache

- vérifiez avec un `ls -al` le dossier du site (dans `/var/www/...`) et tout son contenu appartient au même utilisateur
```
[clement@web share]$ ls -al
[...]
drwxr-xr-x.   2 apache root    24 Sep 29 10:41 testpage
[...]

[clement@web testpage]$ ls -al
total 12
drwxr-xr-x.  2 apache root   24 Sep 29 10:41 .
drwxr-xr-x. 91 apache root 4096 Sep 29 10:41 ..
-rw-r--r--.  1 apache root 7621 Jun 11 17:23 index.html
```
🌞 **Changer l'utilisateur utilisé par Apache**

- créez le nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant
    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut
```
[clement@web ~]$ tail -1  /etc/passwd
myUserApache:x:1001:1001::/usr/share/httpd:/sbin/nologin
```

- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```
[clement@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep user -i
[...]
User myUserApache
[...]
```

- redémarrez Apache
```
[clement@web ~]$ sudo systemctl restart httpd
```

- utilisez une commande `ps` pour vérifier que le changement a pris effet
```
[clement@web ~]$ ps -ef | grep httpd
root        2298       1  0 15:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
myUserA+    2300    2298  0 15:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
myUserA+    2301    2298  0 15:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
myUserA+    2302    2298  0 15:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
myUserA+    2303    2298  0 15:17 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
clement     2527    2221  0 15:19 pts/0    00:00:00 grep --color=auto httpd
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port
```
[clement@web conf]$ cat httpd.conf  | grep "Listen 1234"
Listen 1234
```

- ouvrez un nouveau port firewall, et fermez l'ancien
```
[clement@web conf]$ sudo firewall-cmd --add-port=1234/tcp --permanent
[sudo] password for clement:
success

[clement@web conf]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
```

- redémarrez Apache
```
[clement@web conf]$ sudo firewall-cmd --reload
success

[clement@web conf]$ sudo systemctl restart httpd
```

- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```
[clement@web conf]$ sudo ss -tanp  | grep httpd
LISTEN 0      128                *:1234            *:*    users:(("httpd",pid=2684,fd=4),("httpd",pid=2683,fd=4),("httpd",pid=2682,fd=4),("httpd",pid=2679,fd=4))
```

- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```
PS C:\Users\AUGIER de MOUSSAC> curl 10.102.1.11:1234
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system. If you can
read this page, it means that the software it working correctly.
Just visiting?
[...]
```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

En rentrant ```http://10.102.1.11:1234/``` dans la barre de mon naviguateur j'arrive à joindre mon serveur sur le nouveau port.

📁 **Fichier `/etc/httpd/conf/httpd.conf`** (que vous pouvez renommer si besoin, vu que c'est le même nom que le dernier fichier demandé)

# II. Une stack web plus avancée

## 1. Intro

> Pas mal de blabla nécessaire avant de vous lancer. Lisez bien la partie en entier.

Le serveur web `web.tp2.linux` sera le serveur qui accueillera les clients. C'est sur son IP que les clients devront aller pour visiter le site web.  

La serveur de base de données `db.tp2.linux` sera un serveur uniquement accessible depuis `web.tp2.linux`. Les clients ne pourront pas y accéder. Le serveur de base de données stocke les infos nécessaires au serveur web, pour le bon fonctionnement du site web.

---

Bon j'ai un peu réfléchi et le but pour nous là c'est juste d'avoir un serv web + une db, peu importe ce que c'est le site. J'ai pas envie d'aller deep dans la conf de l'un ou de l'autre avec vous pour le moment. Donc on va installer un truc un peu clé en main : Nextcloud.

En plus c'est utile comme truc : c'est un p'tit serveur pour héberger ses fichiers via une WebUI, style Google Drive. Mais on l'héberge nous-mêmes :)

Il y a [**une doc officielle Rocky Linux** plutôt bien fichue pour l'install de Nextcloud](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/#next-steps), je vous laisse la suivre.

⚠️⚠️⚠️**ATTENTION** lisez bien toute la suite avant de vous lancer dans l'install, **lisez la partie en entier.** Ca vous évitera bien des soucis. ⚠️⚠️⚠️

Dans la doc officielle, on vous fait installer le serveur web (Apache) et la base de données (MariaDB) sur la même machine. **Ce n'est PAS ce que nous voulons : nous voulons avoir chaque service sur une machine dédiée.** Donc vous allez devoir ajuster la doc un petit peu. Rien de bien violent :

- quand on vous parle du serveur web, vous faites ça sur `web.tp2.linux`
- quand on vous parle de la base de données, vous faites ça sur `db.tp2.linux`
- à la fin, quand c'est en place, on vous demande d'aller l'interface Web de nextcloud et d'y saisir l'IP de la base de données, pour que NextCloud puisse s'y connecter. Vous saisirez ici l'IP de `db.tp2.linux` à la place de `localhost`

---
---

**Aussi** dans la doc vous est fourni un lien pour installer MariaDB de façon secure. C'est parfait, suivez-le.  
**Par contre** on ne vous donnez aucune infos sur quelle base de données créer dans le serveur de base de données.  
**Donc** avant de démarrer NextCloud, vous devrez :

- démarrer le service de base de données sur `db.tp2.linux`
- vous connecter au serveur de base de données
- exécutez des commandes SQL pour configurer la base, qu'elle soit utilisable par NextCloud

---
---

**Enfin, quelques tips en vrac pour que vous dérouliez l'install dans de bonnes conditions.** Certains tips vont vous paraître random. Beaucoup moins une fois que vous aurez déroulé la doc :

➜ ***Lisez*** et ***suivez bien toutes les instructions***. Toutes les étapes sont strictement nécessaires.

➜ Vous pouvez récupérer votre **timezone** plus facilement en une simple commande : `timedatectl`.

➜ **Si on vous parle d'un dossier et qu'il n'existe pas, créez-le.** Si vous ne savez quelles permissions lui donner, donnez-lui les mêmes permissions que les autres fichiers/dossiers qui se trouvent dans le même dossier.

➜ **Une fois que vous aurez fini d'installer NextCloud**, vous devez visiter l'interface Web. Vous ouvrirez donc votre navigateur, et vous rendrez à l'URL `http://web.tp2.linux`. La page d'accueil de NextCloud s'affichera. **NE VOUS CONNECTEZ PAS** et passez à l'installation de la base de données. C'est sur cet écran que vous indiquerez à NextCloud comment se connecter à votre base, une fois que vous l'aurez installé.

➜ Dans la doc, il est dit : "As noted earlier, we are using the ***"Apache Sites Enabled"*** procedure found here to configure Apache"

Cette ***"Apache Sites Enabled" procedure*** fait référence à une façon d'organiser le dossier `/etc/httpd` pour pas que ce soit le bordel :

- on a créé cette façon de faire car Apache est souvent utilisé pour héberger plusieurs sites web en même temps
- l'idée c'est de faire
  - un dossier `sites-available/` qui contient un fichier de configuration par site web
  - un dossier `sites-enabled/` qui contient des liens vers les fichiers de `sites-available`
- de cette façon il est plus simple de s'y retrouver :
  - un fichier par site, c'est clean
  - un dossier dédié à la conf des sites, et pas à la conf d'Apache plus générale, c'est clean
  - si on veut mettre un site hors-ligne sans faire de la crasse (commenter des lignes, supprimer la conf liée au site etc.) c'est EZ on a juste a supprimer le lien dans `sites-enabled/` ce qui garde intact le vrai fichier de conf dans `sites-available/`
- bref c'est clean quoi :)

**SAUF QUE** si vous suivez juste la doc, ça va pas fonctionner. En effet, les fichiers dans `sites-enabled/` ne seront jamais lus par défaut. Vous devez donc ajouter la ligne suivante dans le fichier `/etc/httpd/httpd.conf` (tout en bas) :

```bash
IncludeOptional sites-enabled/*
```

## 2. Setup

🖥️ **VM db.tp2.linux**

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | ?           | ?             |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.

### A. Serveur Web et NextCloud

**Créez les 2 machines et déroulez la [📝**checklist**📝](#checklist).**

🌞 Install du serveur Web et de NextCloud sur `web.tp2.linux`

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/cms/cloud_server_using_nextcloud/#next-steps)
  - **uniquement pour le serveur Web + NextCloud**, vous ferez la base de données MariaDB après
  - quand ils parlent de la base de données, juste vous sautez l'étape, on le fait après :)
- je veux dans le rendu **toutes** les commandes réalisées
  - n'oubliez pas la commande `history` qui permet de voir toutes les commandes tapées précédemment

```
[clement@web conf]$ sudo dnf install epel-release

[clement@web conf]$ sudo dnf update

[clement@web conf]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm

[clement@web conf]$ sudo dnf module list php

[clement@web conf]$ sudo dnf module enable php:remi-7.4

[clement@web conf]$ dnf module list php

[clement@web conf]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp

[clement@web httpd]$ mkdir sites-available

[clement@web httpd]$ chmod 777 sites-available/

[clement@web sites-available]$ vim web.tp2.linux

[clement@web sites-available]$ sudo chmod 755 web.tp2.linux

[clement@web sites-available]$ cd ..

[clement@web httpd]$ sudo chmod 755 sites-available/

[clement@web httpd]$ sudo mkdir sites-enabled

[clement@web sites-enabled]$ sudo ln -s /etc/httpd/sites-available/web.tp2.linux /etc/httpd/sites-enabled/

[clement@web www]$ sudo mkdir -p /var/www/sub-domains/web.tp2.linux/html

[clement@web httpd]$ sudo vim conf/httpd.conf

[clement@web ~]$ sudo cat /etc/opt/remi/php74/php.ini | grep "date.timezone ="
date.timezone = Europe/Paris

[clement@web ~]$ ls -al /etc/localtime
lrwxrwxrwx. 1 root root 34 Sep 15 10:36 /etc/localtime -> ../usr/share/zoneinfo/Europe/Paris

[clement@web ~]$ wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
[...]

[clement@web ~]$ unzip  nextcloud-22.2.0.zip

[clement@web ~]$ cd nextcloud/

[clement@web nextcloud]$ sudo cp -Rf * /var/www/sub-domains/web.tp2.linux/html/

[clement@web httpd]$ sudo chown -Rf apache.apache /var/www/sub-domains/web.tp2.linux/html/

[clement@web html]$ systemctl restart httpd

```
Une fois que vous avez la page d'accueil de NextCloud sous les yeux avec votre navigateur Web, **NE VOUS CONNECTEZ PAS** et continuez le TP

📁 **Fichier `/etc/httpd/conf/httpd.conf`**  
📁 **Fichier `/etc/httpd/conf/sites-available/web.tp2.linux`**

### B. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/)
- manipulation 
- je veux dans le rendu **toutes** les commandes réalisées
```
[clement@db ~]$ sudo dnf -y install mariadb-server

[clement@db ~]$ sudo systemctl enable mariadb.service
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.

[clement@db ~]$ systemctl restart mariadb
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to restart 'mariadb.service'.
Authenticating as: clement
Password:
==== AUTHENTICATION COMPLETE ====

[clement@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
[...]

[clement@db ~]$ systemctl enable mariadb

[clement@db ~]$ systemctl start mariadb

[clement@db ~]$ mysql_secure_installation

[clement@db ~]$ systemctl restart mariadb

[clement@db ~]$ sudo mysql -u root -p
```

```
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

```
[clement@db ~]$ ss -a -t
State          Recv-Q         Send-Q                 Local Address:Port                    Peer Address:Port          Process
[...]
LISTEN         0              80                                 *:mysql                              *:*

[clement@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for clement:
success

[clement@db ~]$ sudo firewall-cmd --reload
success
```

- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root`
  - exécutez les commandes SQL suivantes :

```sql
# Création d'un utilisateur dans la base, avec un mot de passe
# L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
# Dans notre cas, c'est l'IP de web.tp2.linux
# "meow" c'est le mot de passe :D
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';

# Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

# On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';

# Actualisation des privilèges
FLUSH PRIVILEGES;
```

> Par défaut, vous avez le droit de vous connectez localement à la base si vous êtes `root`. C'est pour ça que `sudo mysql -u root` fonctionne, sans nous demander de mot de passe. Evidemment, n'importe quelles autres conditions ne permettent pas une connexion aussi facile à la base.

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - vous pouvez utiliser la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
- utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
SHOW DATABASES;
USE <DATABASE_NAME>;
SHOW TABLES;
```


```[clement@web ~]$ mysql -u nextcloud -h 10.102.1.12 -pmeow
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 10
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.006 sec)

MariaDB [(none)]> USE nextcloud;
Database changed

MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)
```


- trouver une commande qui permet de lister tous les utilisateurs de la base de données
```
[clement@db ~]$  mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 12
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.000 sec)
```

> Les utilisateurs de la base de données sont différents des utilisateurs du système Linux sous-jacent. Les utilisateurs de la base définissent des identifiants utilisés pour se connecter à la base afin d'y voir ou d'y modifier des données.

### C. Finaliser l'installation de NextCloud

🌞 sur votre PC

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`
- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
  - c'est possible grâce à la modification de votre fichier `hosts`
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
  - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
  - choisissez "MySQL/MariaDB"
  - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

```
PS C:\Users\AUGIER de MOUSSAC> curl web.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
                    </head>
                    </html>

RawContent        : HTTP/1.1 200 OK
                    Keep-Alive: timeout=5, max=100
                    Connection: Keep-Alive
                    Accept-Ranges: bytes
                    Content-Length: 156
                    Content-Type: text/html; charset=UTF-8
                    Date: Sun, 10 Oct 2021 21:03:48 GMT
                    ETag: "...
Forms             : {}
Headers           : {[Keep-Alive, timeout=5, max=100], [Connection, Keep-Alive], [Accept-Ranges, bytes], [Content-Length, 156]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : System.__ComObject
RawContentLength  : 156
```
🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL

```
[clement@db ~]$  mysql -u root -p

MariaDB [(none)]> SHOW DATABASES;

MariaDB [(none)]> USE nextcloud;

MariaDB [nextcloud]> SHOW TABLES;
[...]
108 rows in set (0.001 sec)
```
Il y a 108 tables