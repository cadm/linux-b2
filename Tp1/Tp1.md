Tp1

## 0. Préparation de la machine
pour connection ssh machine 1 
activer carte enps03

Accès à internet
```
Pour la machine 1
[clement@node1 ~]$ ping 8.8.8.8 -c4
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=22.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=21.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=23.9 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=42.9 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 21.173/27.602/42.920/8.898 ms
```
Pour la machine 2
```
[clement@bastion-ovh1fr ~]$ ping 8.8.8.8 -c4
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=38.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=25.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=24.3 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=46.7 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 24.290/33.869/46.713/9.250 ms
```

Changer adresse ip 
machine 1
```
[clement@node1 ~]$ ifconfig enp0s3 10.10.1.11
SIOCSIFADDR: Operation not permitted
SIOCSIFFLAGS: Operation not permitted
[clement@node1 ~]$ sudo !!
sudo ifconfig enp0s3 10.10.1.11
[sudo] password for clement:
```

```
[clement@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:81:d4:2d brd ff:ff:ff:ff:ff:ff
    inet 10.10.1.11/8 brd 10.255.255.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe81:d42d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:6f:57:be brd ff:ff:ff:ff:ff:ff
    inet 192.168.104.4/24 brd 192.168.104.255 scope global dynamic noprefixroute enp0s8
       valid_lft 1080sec preferred_lft 1080sec
    inet6 fe80::a00:27ff:fe6f:57be/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

machine 2
```
[clement@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4d:88:af brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86328sec preferred_lft 86328sec
    inet6 fe80::a00:27ff:fe4d:88af/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:22:68:14 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe22:6814/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

changer nom machine 1
```
[clement@node1 ~]$ hostname
node1.tp1.b2
```

changer nom machine 2
```
[clement@node2 ~]$ hostname
node2.tp1.b2
```

ping la machine 2 depuis la machine 1
```
[clement@bastion-ovh1fr ~]$ ping 192.168.104.3 -c4
PING 192.168.104.3 (192.168.104.3) 56(84) bytes of data.
64 bytes from 192.168.104.3: icmp_seq=1 ttl=64 time=0.453 ms
64 bytes from 192.168.104.3: icmp_seq=2 ttl=64 time=0.573 ms
64 bytes from 192.168.104.3: icmp_seq=3 ttl=64 time=0.854 ms
64 bytes from 192.168.104.3: icmp_seq=4 ttl=64 time=0.596 ms

--- 192.168.104.3 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3070ms
rtt min/avg/max/mdev = 0.453/0.619/0.854/0.146 ms
```

**utiliser 1.1.1.1 comme serveur dns**
machine 1
```
[clement@node1 ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search auvence.co tp1.b2
nameserver 1.1.1.1
nameserver 10.33.10.2
nameserver 10.33.10.148
nameserver 10.33.10.155
```

Vérifions à l'aide dig
```
[clement@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49113
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 39 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 15 12:30:49 CEST 2021
;; MSG SIZE  rcvd: 53
```

la ligne qui correpond au nom demandé : ```ynov.com.               10800   IN      A       92.243.16.143```


la ligne du serveur qui nous à répondu : ```;; ;; SERVER: 1.1.1.1#53(1.1.1.1)```


machine 2
```
[clement@node2 ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search auvence.co tp1.b2
nameserver 1.1.1.1
nameserver 10.33.10.2
nameserver 10.33.10.148
nameserver 10.33.10.155
```

Vérification avec dig
```
[clement@node2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48374
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 57 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 15 12:33:11 CEST 2021
;; MSG SIZE  rcvd: 53
```

la ligne qui correpond au nom demandé :``` ynov.com.               10800   IN      A       92.243.16.143```

la ligne du serveur qui nous à répondu : ```;; SERVER: 1.1.1.1#53(1.1.1.1)```




**les machines doivent pouvoir se joindre par leurs noms respectifs**
pour machine 1
```
[clement@node1 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.104.3 node2.tp1.b2
 
[clement@node1 ~]$ ping node2.tp1.b2 -c4
PING node2.tp1.b2 (192.168.104.3) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (192.168.104.3): icmp_seq=1 ttl=64 time=0.442 ms
64 bytes from node2.tp1.b2 (192.168.104.3): icmp_seq=2 ttl=64 time=0.405 ms
64 bytes from node2.tp1.b2 (192.168.104.3): icmp_seq=3 ttl=64 time=0.595 ms
64 bytes from node2.tp1.b2 (192.168.104.3): icmp_seq=4 ttl=64 time=0.546 ms

--- node2.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3054ms
rtt min/avg/max/mdev = 0.405/0.497/0.595/0.076 ms
```

pour la machine 2
```
[clement@node2 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.104.4 node1.tp1.b2


[clement@node2 ~]$ ping node1.tp1.b2 -c4
PING node1.tp1.b2 (192.168.104.4) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (192.168.104.4): icmp_seq=1 ttl=64 time=0.451 ms
64 bytes from node1.tp1.b2 (192.168.104.4): icmp_seq=2 ttl=64 time=0.598 ms
64 bytes from node1.tp1.b2 (192.168.104.4): icmp_seq=3 ttl=64 time=0.779 ms
64 bytes from node1.tp1.b2 (192.168.104.4): icmp_seq=4 ttl=64 time=1.06 ms

--- node1.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3106ms
rtt min/avg/max/mdev = 0.451/0.722/1.061/0.228 ms
```

**le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**
Status des règles activespour le firewall : 
```
[clement@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
## I. Utilisateurs

### 1. Création et configuration
**🌞 Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :**

- **le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans /home**

- **le shell de l'utilisateur soit /bin/bash**
```
[clement@node1 ~]$ sudo useradd user1Node1 -m -s /bin/bash
[sudo] password for clement:

```

**🌞 Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.**

On crée notre groupe
```
[clement@node1 ~]$ sudo groupadd admin
```
**Pour permettre à ce groupe d'accéder aux droits root:**

- **il faut modifier le fichier /etc/sudoers**
- **on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur**
- **la commande visudo permet d'éditer le fichier, avec un check de syntaxe avant fermeture**
- **ajouter une ligne basique qui permet au groupe d'avoir tous les droits (inspirez vous de la ligne avec le groupe wheel)**

la ligne ajouté est la suivante 
``` 
%admin        ALL=(ALL)       NOPASSWD: ALL
```

**🌞 Ajouter votre utilisateur à ce groupe admins.**
```
[clement@node1 ~]$ sudo usermod -aG admin user1Node1
```

### 2. SSH

## II. Partitionnement
### 1. Préparation de la VM
**Ajout de deux disques durs à la machine virtuelle, de 3Go chacun**
L'ajout des disk dur ont été effectuer va l'interface graphique de VmBox car je n'ais pas réussi à creé avec la cmd line ```$ sudo pvcreate /dev/sdb```
```
[clement@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
sr1          11:1    1 1024M  0 rom

[clement@node1 ~]$ sudo vgs
  VG  #PV #LV #SN Attr   VSize  VFree
  rl    1   2   0 wz--n- <7.00g     0

[clement@node1 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               1kGc60-R7ly-N17q-oBvN-Ha18-QdEn-0EmOrp
```

### 2. Partitionnement
🌞 Utilisez LVM pour :

- agréger les deux disques en un seul *volume group*
```
[clement@node1 ~]$ sudo vgcreate VG1 /dev/sdb
[sudo] password for clement:
  Physical volume "/dev/sdb" successfully created.
  Volume group "VG1" successfully created
  
  [clement@node1 ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               VG1
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <3.00 GiB
  PE Size               4.00 MiB
  Total PE              767
  Alloc PE / Size       0 / 0
  Free  PE / Size       767 / <3.00 GiB
  VG UUID               AZa3sr-0mGL-vPmM-pTAY-G1Uo-YGbk-wGeXyr

  --- Volume group ---
  VG Name               rl
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <7.00 GiB
  PE Size               4.00 MiB
  Total PE              1791
  Alloc PE / Size       1791 / <7.00 GiB
  Free  PE / Size       0 / 0
  VG UUID               mMT7T3-gYaz-S0Mc-n7o3-F0uU-OGvO-p7Nj1B

[clement@node1 ~]$ sudo vgs
  VG  #PV #LV #SN Attr   VSize  VFree
  VG1   1   0   0 wz--n- <3.00g <3.00g
  rl    1   2   0 wz--n- <7.00g     0

  [clement@node1 ~]$ sudo vgextend VG1 /dev/sdc
  Physical volume "/dev/sdc" successfully created.
  Volume group "VG1" successfully extended

[clement@node1 ~]$ sudo vgs
  VG  #PV #LV #SN Attr   VSize  VFree
  VG1   2   0   0 wz--n-  5.99g 5.99g
  rl    1   2   0 wz--n- <7.00g    0

[clement@node1 ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               VG1
  System ID
  Format                lvm2
  Metadata Areas        2
  Metadata Sequence No  2
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                2
  Act PV                2
  VG Size               5.99 GiB
  PE Size               4.00 MiB
  Total PE              1534
  Alloc PE / Size       0 / 0
  Free  PE / Size       1534 / 5.99 GiB
  VG UUID               AZa3sr-0mGL-vPmM-pTAY-G1Uo-YGbk-wGeXyr

  --- Volume group ---
  VG Name               rl
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <7.00 GiB
  PE Size               4.00 MiB
  Total PE              1791
  Alloc PE / Size       1791 / <7.00 GiB
  Free  PE / Size       0 / 0
  VG UUID               mMT7T3-gYaz-S0Mc-n7o3-F0uU-OGvO-p7Nj1B
```
- créer 3 *logical volumes* de 1 Go chacun
```
[clement@node1 ~]$ sudo lvcreate -L 1G VG1 -n logical_volume_1
[sudo] password for clement:
  Logical volume "logical_volume_1" created.

[clement@node1 ~]$ sudo lvcreate -L 1G VG1 -n logical_volume_2
  Logical volume "logical_volume_2" created.

[clement@node1 ~]$ sudo lvcreate -L 1G VG1 -n logical_volume_3
  Logical volume "logical_volume_3" created.

[clement@node1 ~]$ sudo lvs
  LV               VG  Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  logical_volume_1 VG1 -wi-a-----   1.00g
  logical_volume_2 VG1 -wi-a-----   1.00g
  logical_volume_3 VG1 -wi-a-----   1.00g
  root             rl  -wi-ao----  <6.20g
  swap             rl  -wi-ao---- 820.00m

[clement@node1 ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/VG1/logical_volume_1
  LV Name                logical_volume_1
  VG Name                VG1
  LV UUID                qrRYWb-V5v2-OCHS-2XiE-WsFs-b4J6-xalrg9
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-24 18:47:48 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/VG1/logical_volume_2
  LV Name                logical_volume_2
  VG Name                VG1
  LV UUID                QbeEl5-5Pkx-3iAm-vx42-mFCq-3GLm-ipgm1R
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-24 18:48:00 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:3

  --- Logical volume ---
  LV Path                /dev/VG1/logical_volume_3
  LV Name                logical_volume_3
  VG Name                VG1
  LV UUID                y3Posq-vvNA-jcam-G3nf-8Erd-JAVi-pH2PRs
  LV Write Access        read/write
  LV Creation host, time node1.tp1.b2, 2021-09-24 18:48:04 +0200
  LV Status              available
  # open                 0
  LV Size                1.00 GiB
  Current LE             256
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:4
```

- formater ces partitions en `ext4`
```
[clement@node1 ~]$ sudo mkfs -t ext4 /dev/VG1/logical_volume_1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 7ab522b4-4d73-4307-9376-2c60c11f3d0e
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[clement@node1 ~]$ sudo mkfs -t ext4 /dev/VG1/logical_volume_2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: ea8b675b-524e-405e-bfa2-3307a3265852
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[clement@node1 ~]$ sudo mkfs -t ext4 /dev/VG1/logical_volume_3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: ffe69a34-6469-4076-9ed2-ca376bfe6cb3
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

- monter ces partitions pour qu'elles soient accessibles aux points de montage `/mnt/part1`, `/mnt/part2` et `/mnt/part3`.
```
[clement@node1 ~]$ sudo mkdir /mnt/part1
[clement@node1 ~]$ sudo mount /dev/VG1/logical_volume_1 /mnt/part1

[clement@node1 ~]$ sudo mkdir /mnt/part2
[clement@node1 ~]$ sudo mount /dev/VG1/logical_volume_2 /mnt/part2

[clement@node1 ~]$ sudo mkdir /mnt/part3
[clement@node1 ~]$ sudo mount /dev/VG1/logical_volume_3 /mnt/part3

[clement@node1 ~]$ mount
/dev/mapper/VG1-logical_volume_1 on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/VG1-logical_volume_2 on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/VG1-logical_volume_3 on /mnt/part3 type ext4 (rw,relatime,seclabel)

[clement@node1 ~]$ df -h
/dev/mapper/VG1-logical_volume_1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/VG1-logical_volume_2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/VG1-logical_volume_3  976M  2.6M  907M   1% /mnt/part3
```

**🌞 Grâce au fichier `/etc/fstab`, faites en sorte que cette partition soit montée automatiquement au démarrage du système.**

```
[clement@node1 ~]$ sudo vim /etc/fstab
[clement@node1 ~]$ sudo umount /mnt/part1
[clement@node1 ~]$ sudo mount /dev/VG1/logical_volume_2 /mnt/part2
[clement@node1 ~]$ sudo mount /dev/VG1/logical_volume_3 /mnt/part3
[clement@node1 ~]$ sudo umount /mnt/part2
[clement@node1 ~]$ sudo umount /mnt/part3
[clement@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/part1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part1               : successfully mounted
mount: /mnt/part2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part2               : successfully mounted
mount: /mnt/part3 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/part3               : successfully mounteds
```

## III. Gestion de services
## 1. Interaction avec un service existant
**🌞 Assurez-vous que :**
- **l'unité est démarrée**
- **l'unitée est activée (elle se lance automatiquement au démarrage)**
```
[clement@node1 ~]$ systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Fri 2021-09-24 19:07:00 CEST; 10min ago
     Docs: man:firewalld(1)
 Main PID: 897 (firewalld)
    Tasks: 2 (limit: 4946)
   Memory: 30.4M
   CGroup: /system.slice/firewalld.service
           └─897 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid

[clement@node1 ~]$ systemctl is-active firewalld
active
```

## 2. Création de service
### A. Unité simpliste

**🌞 Créer un fichier qui définit une unité de service `web.service` dans le répertoire `/etc/systemd/system`.**
On ouvre le port 8888
```
[clement@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success0
[clement@node1 system]$ sudo firewall-cmd --reload
success
```
```
[clement@node1 system]$ sudo touch web.service
[sudo] password for clement:
[clement@node1 system]$ sudo nano web.service

[clement@node1 system]$ sudo systemctl daemon-reload

[clement@node1 system]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)

[clement@node1 system]$ sudo systemctl start web
[clement@node1 system]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```

**🌞Une fois le service démarré, assurez-vous que pouvez accéder au serveur web : avec un navigateur ou la commande curl sur l'IP de la VM, port 8888.**
```
[clement@node1 system]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```

### B. Modification de l'unité

**🌞 Créer un utilisateur `web`.**
```
[clement@node1 system]$ sudo useradd web
[sudo] password for clement:

[clement@node1 system]$ sudo passwd web
Changing password for user web.
New password:
BAD PASSWORD: The password is shorter than 8 characters
Retype new password:
passwd: all authentication tokens updated successfully.
```

**🌞 Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses :**

- **`User=` afin de lancer le serveur avec l'utilisateur `web` dédié**
- **`WorkingDirectory=` afin de lancer le serveur depuis un dossier spécifique, choisissez un dossier que vous avez créé dans `/srv`**
```
[clement@node1 srv]$ sudo mkdir dossier_Web_Server
```
- **ces deux clauses sont à positionner dans la section `[Service]` de votre unité**

**🌞 Placer un fichier de votre choix dans le dossier créé dans `/srv` et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur `web`.**
```
[clement@node1 srv]$ sudo chown web dossier_Web_Server/
[clement@node1 dossier_Web_Server]$ sudo chown web toto
```

**🌞 Vérifier le bon fonctionnement avec une commande `curl`**
```
[clement@node1 system]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="toto">toto</a></li>
</ul>
<hr>
</body>
</html>
```